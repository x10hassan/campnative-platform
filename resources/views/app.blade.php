<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Camp Native</title>
    <meta name="description" content="Awsome to camp things around">
    <meta name="author" content="Hassan">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('/theme/css/style.css') }}" type="text/css"  media="all">
    <link rel="stylesheet" href="{{ asset('/theme/css/custom.css') }}" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('/theme/css/tabs.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/theme/css/passport.css') }}" />


    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300italic,400italic,400,300,600,700,900|Varela|Arapey:400,400italic' rel="stylesheet" type='text/css' >
    <!-- JS ================================================== -->

    <script src="{{ asset('/theme/js/jquery.min.js') }}" type="text/javascript"></script>

    <!--[if lt IE 9]>
    <script src="{{ asset('/theme/js/modernizr.custom.11889.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/theme/js/respond.js') }}" type="text/javascript"></script>
    <![endif]-->
    <!-- HTML5 Shiv events (end)-->
    <!-- MEGA MENU -->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{ asset('/theme/images/favicon.ico') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/theme/css/quickform/component.css') }}" />

</head>
<body class="review" data-twttr-rendered="true" cz-shortcut-listen="true">
<div id="wrap">

    <!-- Primary Page Layout
    ================================================== -->
    <!-- Header -->
    <header id="header" class="horizontal-w sm-rgt-mn">
        <div  class="container">
            <div class="col-md-3 col-sm-3 logo-wrap">
                <div class="logo">
                    <a href="#"><img src="{{ asset('/theme/images/homepage/reg-logo.png') }}" width="130" id="img-logo-w1" alt="logo" class="img-logo-w1"></a>
                    <a href="#"><img src="{{ asset('/theme/images/homepage/reg-logo.png') }}" width="130" id="img-logo-w2" alt="logo" class="img-logo-w2"></a>
                    <span class="logo-sticky"><a href="#"><img src="{{ asset('/theme/images/homepage/logo_sticky.png') }}" width="60" id="img-logo-w3" alt="logo" class="img-logo-w3"></a></span>
                </div> <!-- end logo -->
            </div> <!-- end col-md-3 -->
            <nav id="nav-wrap" class="nav-wrap1 col-md-9 col-sm-9">
                <div class="container">
                    <!--<div class="woo-cart-header">
                        <a class="header-cart" href="#"></a>
                        <div class="woo-cart-dropdown">
                            <ul class="cart-list product-list-widget">
                                <li>Sign up and Sign in Buttons!</li>
                            </ul>
                            <a href="#" class="qbutton white view-cart">Logout </a>
                            <span class="total">Status:<span class="amount">Native</span></span>
                        </div>
                    </div> <!-- end woo cart header -->
                    <!--<div id="search-form">
                        <a href="javascript:void(0)" class="search-form-icon"><i id="searchbox-icon" class="fa-search"></i></a>
                        <div id="search-form-box" class="search-form-box">
                            <form action="#" method="get">
                                <input type="text" class="search-text-box" id="search-box" name="s">
                            </form>
                        </div>
                    </div> <!-- end search form -->
                    @if (Auth::check())
                    <div class="woo-cart-header">
                        <a class="header-cart" href="#"></a>
                        <div class="woo-cart-dropdown">
                            <a href="{{ action('Auth\AuthController@getReturnToAdmin') }}" class="qbutton white view-cart">RETURN TO ADMIN</a>
                            <a href="/passport/profile" class="qbutton white view-cart">PASSPORT</a>
                            <a href="/auth/logout" class="qbutton white view-cart">LOGOUT</a>
                            <span class="total">{{ Auth::user()->name }}</span>
                        </div>
                    </div> <!-- end woo cart header -->
                    @endif
                    <ul id="nav">
                        @if (Auth::guest())
                            <li class="current">
                                <a href="/" data-description="start here">
                                    <i class="fa-home"></i>Home
                                </a>
                            </li>
                            <li class="current">
                                <a href="/list-campground" data-description="start here">
                                    <i class="fa-plus"></i>List Campground
                                </a>
                            </li>
                            <li class="current">
                                <a href="/auth/login" data-description="start here">
                                    <i class="fa-sign-in"></i>Login
                                </a>
                            </li>
                            <li class="current">
                                <a href="/auth/register" data-description="start here">
                                    <i class="fa-user"></i>Register
                                </a>
                            </li>
                        @else
                            <li class="current">
                                <a href="/home" data-description="start here">
                                    <i class="fa-home"></i>Home
                                </a>
                            </li>
                            <li class="current">
                                <a href="/campground" data-description="start here">
                                    <i class="fa-list"></i>My Campgrounds
                                </a>
                            </li>
                            <li class="current">
                                <a href="/list-campground" data-description="start here">
                                    <i class="fa-plus"></i>List Campground
                                </a>
                            </li>
                        @endif
                    </ul> <!-- end #nav -->
                </div>
            </nav> <!-- end navigation -->
        </div> <!-- end container -->
    </header> <!-- end header -->

    <!-- begin Content -->
    <hr>
    <section id="main-content" class="container">
        @include('flash::message')
        @yield('content')
    </section> <!-- end container -->
    <!-- end Content -->

<section class="blox dark blox4">
    <hr class="vertical-space4">
    <div class="container">
        <div class="col-sm-12">
            <div class="col-sm-3 brdr-r1">
                <hr class="vertical-space2">
                <img width="237" height="61" src="{{ asset('/theme/images/homepage/homepagelogo1x.png') }}" alt="logo-drk">
                <hr class="vertical-space1">
                <hr class="vertical-space">
            </div> <!-- end col-sm-3 -->
            <div class="col-sm-3 brdr-r1">
                <article class="icon-box3">
                    <i class="fa-anchor"></i>
                    <h4></h4>
                </article>
                <div class="aligncenter custom-footer-menu">
                    <a href="#">About</a>
                    <a href="#">Jobs</a>
                    <a href="#">Partners</a>
                    <a href="#">Press</a>

                </div>
                <hr class="vertical-space1">
            </div> <!-- end col-sm-3 -->
            <div class="col-sm-3 brdr-r1">
                <article class="icon-box3">
                    <i class="fa-phone"></i>
                    <h4>Let's Connect</h4>
                </article>
                <p class="aligncenter">Call us at <strong>(123) 456578</strong></p>
                <p class="aligncenter">and email us:<br>
                    <strong>info@camptnative.com</strong>
                </p>
                <hr class="vertical-space1">
            </div> <!-- end col-sm-3 -->
            <div class="col-md-3">
                <div class="widget">
                    <article class="icon-box3">
                        <i class="fa-share-square"></i>
                        <h4>Let's get Social</h4>
                    </article>
                    <div class="socialfollow">
                        <a href="#" class="twitter"><i class="fa-twitter"></i></a><a href="#" class="facebook"><i class="fa-facebook"></i></a><a href="#" class="dribble"><i class="fa-dribbble"></i></a><a href="#" class="google"><i class="fa-google"></i></a><a href="#" class="instagram"><i class="fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div> <!-- end col-sm-12 -->
    </div> <!-- end container -->
</section>

<span id="scroll-top"><a class="scrollup"><i class="fa-chevron-up"></i></a></span>

</div> <!-- end Wrap -->

<!-- End Document
================================================== -->

<!-- JS
================================================== -->
<script type="text/javascript" src="{{ asset('/theme/js/jquery.plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('/theme/js/jquery.masonry.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/theme/js/mediaelement-and-player.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/theme/js/isotope.js') }}"></script>
<script src="{{ asset('/theme/js/sticky-custom.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('/theme/js/campnative-custom.js') }}"></script>
<script src="{{ asset('/theme/js/modernizr.custom.js') }}"></script>
<script src="{{ asset('/theme/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<!-- page level js which only aplies to this page -->
<script src="{{ asset('/theme/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('/theme/js/bootstrap.js') }}"></script>
<script src="{{ asset('/theme/js/bootbox.js') }}" type="text/javascript"></script>
<script src="{{ asset('/theme/js/jquery.touchSwipe.min.js') }}"></script>
<script src="{{ asset('/theme/js/jquery.slimscroll.min.js') }}"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=true&amp;libraries=geometry&amp;libraries=places" type="text/javascript"></script>
<script src="{{ asset('/theme/js/infobox.js') }}"></script>
<script src="{{ asset('/theme/js/jquery.tagsinput.min.js') }}"></script>
<script src="{{ asset('/theme/js/fileinput.min.js') }}"></script>
<script src="{{ asset('/theme/js/app-list.js') }}"></script>

<script>
    $('#flash-overlay-modal').modal();
</script>

</body>
</html>