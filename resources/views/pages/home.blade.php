@extends('app')

@section('content')
    <hr class="vertical-space1">
    <div class="col-sm-7 align-center">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    <h3>Home</h3>
                    <p>
                        <small>
                            Welcome, {{ Auth::user()->name }}!
                        </small>
                    </p>
                </div>
            </div>
        </div>
    </div> <!-- end col-sm-8 -->
@endsection
