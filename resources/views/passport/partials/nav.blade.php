<div class="tabs tabs-style-flip-1">
    <nav>
        <ul>
            <li><a href="{{ action('PassportController@getProfile') }}"><i class="fa fa-home fa-2x"></i> <span>Profile</span></a></li>
            <li><a href="{{ action('PassportController@getProfile') }}"><i class="fa fa-compass fa-2x"></i> <span>Visited Areas</span></a></li>
            <li><a href="{{ action('PassportController@getProfile') }}"><i class="fa fa-heart fa-2x"></i> <span>Dream Campsites</span></a></li>
            <li><a href="{{ action('PassportController@getSettings') }}"><i class="fa fa-cog fa-2x"></i> <span>Settings</span></a></li>
        </ul>
    </nav>

    <hr>
</div>