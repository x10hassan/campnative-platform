@extends('app')

@section('content')
    @include('passport.partials.nav')

    <hr class="vertical-space1">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    <h3>Edit Account Settings</h3>
                    <p>You can edit your account below.</p>
                    @include('messages.errors')

                    <form class="p-t-15" role="form" enctype="multipart/form-data" method="POST" action="/passport/settings">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label for="name">Your Name</label>
                                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name') ?: Auth::user()->name }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label for="email">E-mail Address</label>
                                    <input type="text" id="email" name="email" class="form-control" value="{{ old('email') ?: Auth::user()->email }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label for="password">New Password</label>
                                    <input type="password" id="password" name="password" class="form-control" placeholder="Choose a new password (optional)">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label for="password_confirmation">Confirm Password</label>
                                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="Confirm New Password (required if changing password)">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group form-group-default aligncenter">
                                    <label for="avatar">Current Image:</label>
                                        <img src="{{ asset('/uploads/avatars/'.Auth::user()->avatar_file) }}" alt="" />
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group form-group-default">
                                    <label for="avatar">Choose Your Avatar</label>
                                    <input type="file" name="avatar" id="avatar">
                                </div>
                            </div>
                        </div>

                        <div class="row pull-right">
                            <button type="submit" class="text-center btn btn-primary m-t-05">Update Settings</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
@endsection
