@extends('app')

@section('content')

    @include('passport.partials.nav')

	<section id="section--flip-1">
         <hr class="vertical-space1">
            <div class="col-sm-4">
               <article class="our-team">
                  <figure><img src="{{ asset('/uploads/avatars/'.Auth::user()->avatar_file) }}" alt=""></figure>
                  <h2>{{ Auth::user()->name }}</h2>
                  <hr class="vertical-space">
                     <div class="col-xs-6">
                        <h4>Native</h4>
                     </div>
                     <div class="col-xs-6">
                        <a href="{{ action('Auth\AuthController@getLogout') }}" class="logoutbutton">Logout</a>
                     </div>
               </article> <!-- end of profile man -->

            </div> <!-- end col-sm-8 -->
       
            <div class="col-sm-8">
               <article class="callout">
                  Welcome
                  <a class="callurl" href="#">
                     <i class="fa fa-facebook-square fa-lg"></i> Connect</a>
                  <h3>{{ Auth::user()->name }}, </h3>
                  <hr>
                  <div class="sub-content">
                     <h6 class="h-sub-content">Badges</h6>
                  </div
                  <ul id="our-clients" class="our-clients ">
                  	TODO Badges are based on the review system
                     <!-- <li><img src="public/theme/images/passport/badges/sample.png" alt=""></li>
                     <li><img src="public/theme/images/passport/badges/sample.png" alt=""></li>
                     <li><img src="public/theme/images/passport/badges/sample.png" alt=""></li>
                     <li><img src="public/theme/images/passport/badges/sample.png" alt=""></li>
                     <li><img src="public/theme/images/passport/badges/sample.png" alt=""></li>
                     <li><img src="public/theme/images/passport/badges/sample.png" alt=""></li>
                     <li><img src="public/theme/images/passport/badges/sample.png" alt=""></li>
                     <li><img src="public/theme/images/passport/badges/sample.png" alt=""></li>
                     <li><img src="public/theme/images/passport/badges/sample.png" alt=""></li>
                     <li><img src="public/theme/images/passport/badges/sample.png" alt=""></li> -->
                  </ul>
                  <div class="sub-content">
                     <h6 class="h-sub-content">Stamps</h6>
                  </div>
                     <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">×</button>Heads up! You have never been to anywhere you'll get one once you do!  </div>
               </article> <!-- end of callout -->
            </div> <!-- end col-sm-8 -->
      </section>
@endsection