@extends('app')

@section('content')
    <hr class="vertical-space1">
    <div class="col-sm-7 align-center">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    <h3>Reset Password</h3>
                    <p>
                        <small>
                            Forgot your password? Send a password reset link to your email address.
                        </small>
                    </p>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="p-t-15" role="form" method="POST" action="{{ url('/password/email') }}" novalidate="novalidate">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>E-Mail Address</label>
                                    <input type="text" name="email" placeholder="Email" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <button type="submit" class="text-center btn btn-primary m-t-05">Send Password Reset Link</button>
                        </div>
                    </form></div>
            </div>
        </div>
    </div> <!-- end col-sm-8 -->
@endsection