@extends('app')

@section('content')
    <section id="main-content" class="container">
        <!-- Start Page Content -->
        <hr class="vertical-space1">
        <div class="col-sm-7 align-center">
            <div class="container-sm-height full-height">
                <div class="row row-sm-height">
                    <div class="col-sm-12 col-sm-height col-middle">
                        <h3>Login</h3>

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="text" name="email" placeholder="Email" class="form-control" required="" aria-required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>Password</label>
                                    <input type="password" name="password" placeholder="Password" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                            </div>
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Login</button>

                                <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> <!-- end col-sm-8 -->

    </section>
@endsection
