@extends('app')

@section('content')
    <hr class="vertical-space1">
    <div class="col-sm-7 align-center">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    <h3>Register</h3>
                    <p>
                        <small>
                            Register for a FREE account now. Already have one? <a href="/auth/login">Login here</a>.
                        </small>
                    </p>

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="p-t-15" role="form" method="POST" action="{{ url('/auth/register') }}" novalidate="novalidate">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Full Name</label>
                                    <input type="text" name="name" placeholder="Your Name" class="form-control" required="" value="{{ old('name') }}" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>E-Mail Address</label>
                                    <input type="text" name="email" placeholder="Your Email" class="form-control" required="" value="{{ old('email') }}" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Password</label>
                                    <input type="password" name="password" placeholder="Password" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Repeat Password</label>
                                    <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <br />
                                <hr />
                                <center>By clicking the Register button, you are agreeing to our <a href="/terms" target="_blank">Terms and Conditions</a>.</center>
                                <hr />
                                <br />
                            </div>
                        </div>
                        <div class="row pull-right">
                            <button type="submit" class="text-center btn btn-primary m-t-05">Register</button>
                        </div>
                    </form></div>
            </div>
        </div>
    </div> <!-- end col-sm-8 -->
@endsection