@extends('app')

@section('content')
    <hr class="vertical-space1">
    <div class="col-sm-7 align-center">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                <h3>Login</h3>
                <p>
                    <small>
                        Login into your account.
                    </small>
                </p>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="p-t-15" role="form" method="POST" action="{{ url('/auth/login') }}" novalidate="novalidate">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>E-Mail Address</label>
                                <input type="text" name="email" placeholder="Email" class="form-control" required="" aria-required="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Password</label>
                                <input type="password" name="password" placeholder="Password" class="form-control" required="" aria-required="true">
                            </div>
                        </div>
                    </div>
                    <div class="row pull-right">
                        <button type="submit" class="text-center btn btn-primary m-t-05">Login</button>
                        <a href="{{ url('/login/fb') }}" class="button medium"><i class="fa fa-facebook"></i>Connect</a>

                        <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
                    </div>
                </form></div>
            </div>
        </div>
    </div> <!-- end col-sm-8 -->
@endsection
