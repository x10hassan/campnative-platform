@extends('admin.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Admin - CampGrounds</div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table user-list">
                                <thead>
                                <tr>
                                    <th><span>Name</span></th>
                                    <th><span>Email</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($campgrounds as $campground)
                                    <tr>
                                        <td>{{ $campground->name }}</td>
                                        <td>{{ $campground->email }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="2" style="text-align:center;">There aren't any campgrounds created.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="pull-right">
                        {!! $campgrounds->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
