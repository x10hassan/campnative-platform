@extends('admin.app')

@section('content')
	<div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">There are currently {{ count($users) }} active users. Here you can manage, create, and delete users with a click of a button.</div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table user-list">
                                <thead>
                                <tr>
                                    <th><span>Name</span></th>
                                    <th><span>Email</span></th>
                                    <th><span>Last active</span></th>
                                    <th><span>Created</span></th>
                                    <th><span>Operations</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->getLastLogin() }}</td>
                                        <td>{{ $user->created_at->diffForHumans() }}</td>
                                        <td>
                                            <a href=""><i class="fa fa-user"></i> Manage account </a> | 
                                            <a href="" data-toggle="modal" data-target="#deleteUser{{ $user->id }}"><i class="fa fa-trash-o"></i> Delete </a>
                                            <div class="modal fade" id="deleteUser{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteUserModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                            <h4 class="modal-title" id="deleteCategoryModalLabel">Are you sure you want to delete {{ $user->name }} ({{ $user->email }})?</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            Deleting this user is an irreversible action. Are you sure you want to continue?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <form method="POST" action="{{ action('Admin\UsersController@destroy', [$user->id]) }}">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <input name="_method" type="hidden" value="DELETE">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                <button type="submit" class="btn btn-danger">
                                                                Yes, delete
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="2">There aren't any users created.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pull-right">
                        {{ $users->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endsection