@extends('admin.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">There are currently {!! count($users) !!} active users.</div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table user-list">
                                <thead>
                                <tr>
                                    <th><span>Name</span></th>
                                    <th><span>Email</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="2">There aren't any users created.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">There {!! count($campgrounds) > 1 ? 'are' : 'is' !!} currently {!! count($campgrounds) !!} active {!! count($campgrounds) > 1 ? 'campgrounds' : 'campground' !!}.</div>
                                        <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table user-list">
                                <thead>
                                <tr>
                                    <th><span>Campground</span></th>
                                    <th><span>Address</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($campgrounds as $camp)
                                    <tr>
                                        <td>{{ $camp->name }}</td>
                                        <td>{{ $camp->address }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="2">There aren't any campgrounds as of now.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
