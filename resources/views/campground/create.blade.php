@extends('app')

@section('content')

    <div class="col-sm-5">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    <h3>List CampGround</h3>
                    <p>
                        <small>
                            Create a CampGround. If you have seen your campground already on our site please claim it instead of listing a new one!
                        </small>
                    </p>

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="p-t-15" role="form" method="POST" action="{{ action('CampgroundController@store') }}" novalidate="novalidate">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Name</label>
                                    <input type="text" name="name" placeholder="Your CampGround Name (this can be changed later)" value="{{ old('name') }}" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>
                                        Address
                                        ( <span id="latitude" class="label label-default"></span> <span id="longitude" class="label label-default"></span> )
                                    </label>
                                    <input type="text" id="address" name="address" placeholder="The CampGround Address" value="{{ old('address') }}" autocomplete="off" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Phone</label>
                                    <input type="text" name="phone" placeholder="The CampGround Phone Number" value="{{ old('phone') }}" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Email</label>
                                    <input type="text" name="email" placeholder="The CampGround E-Mail Address" value="{{ old('email') }}" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <br />
                                <hr />
                                <center>By clicking the Create CampGround button, you are agreeing to our <a href="/terms" target="_blank">Terms and Conditions</a>.</center>
                                <hr />
                                <br />
                            </div>
                        </div>
                        <div class="row pull-right">
                            <button type="submit" class="text-center btn btn-primary m-t-05">Create CampGround</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> <!-- end col-sm-8 -->

    <div class="col-sm-7">
        <div id="mapView" class="mob-min">
            <div class="mapPlaceholder"><span class="fa fa-spin fa-spinner"></span> Loading map...
            </div>
        </div>
    </div> <!-- end col-sm-8 -->

    </section>
    <hr class="vertical-space4">
@endsection