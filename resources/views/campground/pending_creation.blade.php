@extends('app')

@section('content')
    <hr class="vertical-space1">
    <div class="col-sm-10 align-center">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    <h3>Your CampGround has not been created yet..</h3>
                    <p>
                        Once you login or register, your campground will be created and you'll be redirected to your campground page.
                    </p>
                    <p>
                        <a href="{{ action('Auth\AuthController@getLogin') }}">Login</a> OR
                        <a href="{{ action('Auth\AuthController@getRegister') }}">Register</a>
                    </p>
                </div>
            </div>
        </div>
    </div> <!-- end col-sm-8 -->
@endsection