@extends('app')

@section('content')
    @include('campground.partials.nav', ['campground' => $campground])

    <hr class="vertical-space1">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    @include('campground.partials.title', ['page' => 'Create CampSite', 'name' => $campground->name])
                    <p>You can add CampSites to this CampGround below.</p>
                    @include('messages.errors')

                    <form class="p-t-15" role="form" method="POST" action="{{ action('CampgroundCampsiteController@store', [$campground->id]) }}" novalidate="novalidate">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Name</label>
                                    <input type="text" name="name" placeholder="Your CampSite Name (this can be changed later)" value="{{ old('name') }}" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Address</label>
                                    <input type="text" name="address" placeholder="The CampSite Address" value="{{ old('address') }}" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Phone</label>
                                    <input type="text" name="phone" placeholder="The CampSite Phone Number" value="{{ old('phone') }}" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Email</label>
                                    <input type="text" name="email" placeholder="The CampSite E-Mail Address" value="{{ old('email') }}" class="form-control" required="" aria-required="true">
                                </div>
                            </div>
                        </div>

                        <div class="row pull-right">
                            <button type="submit" class="text-center btn btn-primary m-t-05">Create CampSite</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
