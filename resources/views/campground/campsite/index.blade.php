@extends('app')

@section('content')
    @include('campground.partials.nav', ['campground' => $campground])

    <hr class="vertical-space1">

    Add campsite button/link

    <div class="col-sm-10 col-sm-offset-1">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    @include('campground.partials.title', ['page' => 'CampSites', 'name' => $campground->name])
                    <div class="table-responsive">
                        <table class="table user-list">
                            <thead>
                            <tr>
                                <th><span>Name</span></th>
                                <th><span>Created</span></th>
                                <th><span></span></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($campsites as $campsite)
                                <tr>
                                    <td>{{ $campsite->name }}</td>
                                    <td>{{ $campsite->created_at->diffForHumans() }}</td>
                                    <td>
                                        <a href="{{ action('CampgroundCampsiteController@delete', [$campground->id, $campsite->id]) }}">Delete <i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" style="text-align:center;">You don't have any campsites created for this campground. Create one using the above form.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        <div class="pull-right">
                            {!! $campsites->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
