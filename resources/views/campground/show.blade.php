@extends('app')

@section('content')
	<div class="tabs tabs-style-flip-1">
	    <nav>
	        <ul>
	            <li><a href="{{ action('PassportController@getProfile') }}"><i class="fa fa-home fa-2x"></i> <span>CampGround</span></a></li>
	            <li><a href="{{ action('PassportController@getProfile') }}"><i class="fa fa-photo fa-2x"></i> <span>Pictures</span></a></li>
	            <li><a href="{{ action('PassportController@getProfile') }}"><i class="fa fa-heart fa-2x"></i> <span>Campsites</span></a></li>
	            <li><a href="{{ action('PassportController@getSettings') }}"><i class="fa fa-star fa-2x"></i> <span>Reviews</span></a></li>
	        </ul>
	    </nav>

	    <hr>
	</div>

	<section id="photos" class="blox blox1">
		<div class="container">
		      <div class="col-sm-12">
		         <section class="latest-works col3-w with-space-w">
		             <div class="max-title5">
		                <h2>Photo Gallery</h2>
		             </div> <!-- end max-title2 -->
		         
		             <div class="portfolio  isotope" style="position: relative; overflow: hidden; height: 576px;">
		               @foreach($photos as $photo)
		               <figure class="portfolio-item  solid   isotope-item" style="position: absolute; left: 0px; top: 0px; -webkit-transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1); opacity: 1;">
		                  <div class="img-item">
		                     <img src="{{ asset('/uploads/campground/photos/'.$campground->id.'/'.$photo->name) }}" alt="Gallery photo 12" class="Full portfolio_full ">
		                     <div class="zoomex2">
		                        <span>{!! $photo->created_at !!}</span>
		                        <h6><a href="portfolio-item.html"><a href="{{ asset('/uploads/campground/photos/'.$campground->id.'/'.$photo->name) }}" class="prettyPhoto zoomlink1"><i class="fa-search fa-3x"></i></a></a></h6>
		                     </div>
		                  </div>
		               </figure> <!-- end gallery-item -->
		               @endforeach
		            </div> <!-- end portfolio  isotope -->
		         </section> <!-- end latest-photos -->
		         <hr id="sites" class="vertical-space2">
		      </div> <!-- end col-sm-12 -->
		   </div>
	</section>

   <section class="campgroundhero blox2">
      <div class="container">
          <div class="col-sm-12">
             <div class="col-sm-6"></div>
             <div class="col-sm-6 aligncenter">
               
               <!-- something can go over the featured image -->

             </div> <!-- end col-sm-6 -->
          </div> <!-- end col-sm-12 -->
       </div> <!-- end container -->
   </section>

   <section id="ground" id="headline">
      <div class="container">
         <div class="col-wks-1">
            <div class="works-item-date-box">
               <div class="container">
   <!-- Rating System
   ================================================== -->
               <fieldset class="rating">
                   <input type="radio" id="star5" name="rating" value="5"><label class="full" for="star5" title="Awesome - 5 stars"></label>
                   <input type="radio" id="star4half" name="rating" value="4 and a half"><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                   <input type="radio" id="star4" name="rating" value="4"><label class="full" for="star4" title="Pretty good - 4 stars"></label>
                   <input type="radio" id="star3half" name="rating" value="3 and a half"><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                   <input type="radio" id="star3" name="rating" value="3"><label class="full" for="star3" title="Meh - 3 stars"></label>
                   <input type="radio" id="star2half" name="rating" value="2 and a half"><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                   <input type="radio" id="star2" name="rating" value="2"><label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                   <input type="radio" id="star1half" name="rating" value="1 and a half"><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                   <input type="radio" id="star1" name="rating" value="1"><label class="full" for="star1" title="Sucks big time - 1 star"></label>
                   <input type="radio" id="starhalf" name="rating" value="half"><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
               </fieldset>
               </div>
            </div>
         </div> <!-- end col-wks-1 -->
         <div class="col-wks-2 aligncenter">
            <h1 class="portfolio-item-title">American Camping Association</h1>
         </div> <!-- end col-wks-2 -->
         <div class="col-wks-3">
            <div class="works-item-cat-box">
               <span class="wrks-itm-cat"> <a href="#"><i class="fa fa-plus-circle"></i> I've been here</a></span> </br><span class="wrks-itm-cat"> <a href="#"><i class="fa fa-plus-circle"></i> add to dream list</a></span>
            </div>
         </div> <!-- end col-wks-3 -->
      </div>
   </section> <!-- end headline -->

   <section class="blox blox1">
      <div class="container">
          <div class="col-sm-12">
             <hr class="vertical-space">
             <article class="icon-box7">
                <i class="fa-map-marker"></i>
                <h4>{!! $campground->address !!}</h4>
             </article>
             <div class="w-google-map">
                <iframe height="490" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=london&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=39.047881,86.044922&amp;ie=UTF8&amp;hq=&amp;hnear=London,+United+Kingdom&amp;t=m&amp;ll=51.508101,-0.128059&amp;spn=0.07479,0.21286&amp;z=12&amp;iwloc=A&amp;output=embed"></iframe>      
             </div>
             <hr class="vertical-space1">
          </div> <!-- end col-sm-12 -->
       </div> <!-- end container -->
   </section> <!-- end blox -->

@endsection