@extends('app')


@section('content')
    <hr class="vertical-space1">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    <h3><span>My CampGrounds</span></h3>
                    <div class="table-responsive">
                        <table class="table user-list">
                            <thead>
                            <tr>
                                <th><span>Name</span></th>
                                <th><span>Created</span></th>
                                <th><span></span></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($campgrounds as $campground)
                                <tr>
                                    <td><a href="{{ action('CampgroundController@show', [$campground->id]) }}">{{ $campground->name }}</a></td>
                                    <td>{{ $campground->created_at->diffForHumans() }}</td>
                                    <td style="text-align:center;">
                                        <a href="{{ action('CampgroundController@show', [$campground->id]) }}">View <i class="fa fa-eye"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="{{ action('CampgroundController@edit', [$campground->id]) }}">Edit <i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="{{ action('CampgroundPhotoController@index', [$campground->id]) }}">Photos <i class="fa fa-picture-o"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="#" data-bb="confirm" class="btn btn-default">Delete <i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" style="text-align:center;">You haven't created any campgrounds. <a href="{{ action('CampgroundController@create') }}">Create one now?</a></td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        <div class="pull-right">
                            {!! $campgrounds->render() !!}
                        </div>
                    </div>
                </div>
            </div>
    </div>

@endsection