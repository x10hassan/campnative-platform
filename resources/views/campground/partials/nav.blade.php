<div class="tabs tabs-style-flip-1">
    <nav>
        <ul>
            <li><a href="{{ action('CampgroundController@show', [$campground->id]) }}"><i class="fa fa-home fa-2x"></i> <span>Public Page</span></a></li>
            <li><a href="{{ action('CampgroundPhotoController@index', [$campground->id]) }}"><i class="fa fa-picture-o fa-2x"></i> <span>Photos</span></a></li>
            <li><a href="{{ action('CampgroundController@edit', [$campground->id]) }}"><i class="fa fa-cog fa-2x"></i> <span>Edit</span></a></li>
            <li><a href="{{ action('CampgroundController@edit', [$campground->id]) }}"><i class="fa fa-trash-o fa-2x"></i> <span>Delete</span></a></li>
        </ul>
    </nav>

    <hr>
</div>