@extends('app')

@section('content')
    @include('campground.partials.nav', ['campground' => $campground])

    <hr class="vertical-space1">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    @include('campground.partials.title', ['page' => 'Edit CampGround', 'name' => $campground->name])
                    <p>You can edit your campground details below.</p>
                    @include('messages.errors')

                    <form class="p-t-15" role="form" enctype="multipart/form-data" method="POST" action="{{ action('CampgroundController@update', [$campground->id]) }}">
                        <input name="_method" type="hidden" value="PATCH">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label for="name">Name</label>
                                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name') ?: $campground->name }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label for="address">CampGround Address</label>
                                    <input type="text" id="address" name="address" class="form-control" value="{{ old('address') ?: $campground->address }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label for="email">E-mail Address</label>
                                    <input type="text" id="email" name="email" class="form-control" value="{{ old('email') ?: $campground->email }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label for="phone">Phone Number</label>
                                    <input type="text" id="phone" name="phone" class="form-control" value="{{ old('phone') ?: $campground->phone }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label for="description">Description</label>
                                    <input type="text" id="description" name="description" class="form-control" value="{{ old('description') ?: $campground->description }}">
                                </div>
                            </div>
                        </div>

                        <div class="row pull-right">
                            <button type="submit" class="text-center btn btn-primary m-t-05">Edit CampGround</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
@endsection
