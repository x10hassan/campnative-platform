@extends('app')

@section('content')
    @include('campground.partials.nav', ['campground' => $campground])

    <hr class="vertical-space1">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    @include('campground.partials.title', ['page' => 'Upload Photo', 'name' => $campground->name])
                    <p>Upload a new photo for this campground.</p>
                    @include('messages.errors')

                    <form class="p-t-15" role="form" enctype="multipart/form-data" method="POST" action="{{ action('CampgroundPhotoController@store', [$campground->id]) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label for="photo">Choose Your Image</label>
                                    <input type="file" name="photo" id="photo">
                                </div>
                            </div>
                        </div>

                        <div class="row pull-right">
                            <button type="submit" class="text-center btn btn-primary m-t-05">Upload Photo</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-10 col-sm-offset-1">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    <h3><span>Uploaded CampGround Photos</span></h3>
                    <div class="table-responsive">
                        <table class="table user-list">
                            <thead>
                            <tr>
                                <th><span>Preview</span></th>
                                <th><span>Uploaded</span></th>
                                <th><span></span></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($photos as $photo)
                                <tr>
                                    <td><img src="{{ asset('/uploads/campground/photos/'.$campground->id.'/'.$photo->name) }}" alt="" style="width:150px;height:100px;"></td>
                                    <td>{{ $photo->created_at->diffForHumans() }}</td>
                                    <td>
                                        <a href="{{ action('CampgroundPhotoController@delete', [$campground->id, $photo->id]) }}">Delete <i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" style="text-align:center;">You don't have any photos uploaded for this campground. Upload one using the above form.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        <div class="pull-right">
                            {!! $photos->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
