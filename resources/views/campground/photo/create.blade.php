@extends('app')

@section('content')
    @include('campground.partials.nav', ['campground' => $campground])

    <hr class="vertical-space1">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="container-sm-height full-height">
            <div class="row row-sm-height">
                <div class="col-sm-12 col-sm-height col-middle">
                    @include('campground.partials.title', ['page' => 'Upload Photo', 'name' => $campground->name])
                    <p>Upload a new photo for this campground.</p>
                    @include('messages.errors')

                    <form class="p-t-15" role="form" enctype="multipart/form-data" method="POST" action="{{ action('CampgroundPhotoController@store', [$campground->id]) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label for="photo">Choose Your Image</label>
                                    <input type="file" name="photo" id="photo">
                                </div>
                            </div>
                        </div>

                        <div class="row pull-right">
                            <button type="submit" class="text-center btn btn-primary m-t-05">Upload Photo</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
