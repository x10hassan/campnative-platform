<?php namespace CampNative;

use Illuminate\Database\Eloquent\Model;

class CampgroundPhoto extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'campground_photos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['name'];

    /**
     * Photo belongs to a campground relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campground()
    {
        return $this->belongsTo('CampNative\Campground');
    }

    /**
     * Scopes the eloquent query for a specific campground
     *
     * @param $query
     * @param $userId
     * @return mixed
     */
    public function scopeWhereUser($query, $userId)
    {
        return $query->with(['campground' => function($query) use ($userId) {
            $query->whereUserId($userId);
        }]);
    }

}
