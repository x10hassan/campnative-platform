<?php namespace CampNative;

use Illuminate\Database\Eloquent\Model;

class CampgroundCampsite extends Model {

    /**
     * Database table
     *
     * @var string
     */
	public $table = 'campground_campsites';

    /**
     * Mass assignment fillable columns
     *
     * @var array
     */
    public $fillable = [
        'name',
        'address',
        'phone',
        'email',
        'description'
    ];

    /**
     * Campsite belongs to a campground relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campground()
    {
        return $this->belongsTo('CampNative\Campground');
    }

    /**
     * Scopes the eloquent query for a specific campground
     *
     * @param $query
     * @param $userId
     * @return mixed
     */
    public function scopeWhereUser($query, $userId)
    {
        return $query->with(['campground' => function($query) use ($userId) {
            $query->whereUserId($userId);
        }]);
    }

}
