<?php namespace CampNative\Http\Controllers;

use CampNative\Campground;
use CampNative\CampgroundPhoto;
use CampNative\Http\Requests;
use CampNative\Http\Controllers\Controller;

use CampNative\Services\CampgroundManager;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Session\Store;

class CampgroundController extends Controller {

    /**
     * @var Guard
     */
    public $auth;

    /**
     * @var CampgroundManager
     */
    public $manager;

    /**
     * New class instance
     *
     * @param Guard $auth
     * @param CampgroundManager $manager
     */
    public function __construct(Guard $auth, CampgroundManager $manager)
    {
        $this->auth = $auth;
        $this->manager = $manager;

        $options = [
            'only' => [
                'index',
                'show',
                'edit',
                'update'
            ],
        ];

        $this->middleware('auth', $options);
        $this->middleware('hasUserRole', $options);
    }

    /**
     * List Campground page/form
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('campground.create');
    }

    /**
     * Stores the inputed data into session after validation.
     * Shows a page that tells the user they need to login or register IF they are
     * not authenticated. Otherwise, creates the Campground and redirects to campground/manage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->manager->rules());

        // Store the input to session so we can handle it later
        $this->manager->storeToSession($request->only([
            'name', 'address', 'phone', 'email'
        ]));

        // If the user is a guest then just render the pending creation view
        if ($this->auth->guest()) {
            return view('campground.pending_creation');
        }

        return $this->storeAndRedirect();
    }

    /**
     * Store to database from session and redirects to management pages.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeAndRedirect()
    {
        // Store to database
        $campground = $this->manager->createFromSession();

        flash()->success('You have successfully created this CampGround! You can modify it using the links below.');

        return redirect()->action('CampgroundController@edit', [$campground->id]);
    }

    /**
     * Stores the longitude and latitude in a session for use later
     * when creating a campground in the database.
     *
     * @param Request $request
     * @return string
     */
    public function storeLongAndLatSessions(Request $request)
    {
        session(['location_long' => $request->get('location_long')]);
        session(['location_lat' => $request->get('location_lat')]);

        return 'success';
    }

    /**
     * List the users campgrounds
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $campgrounds = Campground::whereUser($this->auth->user()->id)
            ->latest()
            ->paginate(1);

        return view('campground.index', compact('campgrounds'));
    }

    /**
     * Show Campground page
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $campground = $this->manager->find($id);
        $photos = CampgroundPhoto::whereUser($this->auth->user()->id)
            ->whereCampgroundId($campground->id)
            ->latest()
            ->paginate(5);

        return view('campground.show', compact('campground', 'photos'));
    }

    /**
     * Edit Campground page/form
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $campground = $this->manager->find($id);

        return view('campground.edit', compact('campground'));
    }

    /**
     * Updates a specific campground
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $campground = $this->manager->find($id);

        // Validate the form input
        $this->validate($request, $this->manager->rules());

        // Update the campground
        $campground->update($request->only([
            'name', 'address', 'phone', 'email'
        ]));

        // Flash and redirect
        flash()->success('You have successfully updated this CampGround.');
        return redirect()->action('CampgroundController@edit', [$id]);
    }

    public function delete($id) 
    {
        $this->manager->delete($id);
        flash()->success('You have successfully deleted this CampGround');
        return redirect()->action('CampgroundController@index');
    }

}
