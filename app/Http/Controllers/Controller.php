<?php namespace CampNative\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Laracasts\Flash\Flash;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

    /**
     * @var Flash
     */
    public $flash;

    public function __construct(Flash $flash)
    {
        $this->flash = $flash;
    }

}
