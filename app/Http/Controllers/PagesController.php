<?php namespace CampNative\Http\Controllers;

use CampNative\Http\Controllers\Controller;

class PagesController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('hasUserRole');
    }

    public function home()
    {
        return view('pages.home');
    }

    /**
     * Terms page/view
     *
     * @return \Illuminate\View\View
     */
    public function terms()
    {
        return view('pages.terms');
    }

    /**
     * Privacy page/view
     *
     * @return \Illuminate\View\View
     */
    public function privacy()
    {
        return view('pages.privacy');
    }

}
