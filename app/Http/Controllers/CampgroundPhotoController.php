<?php namespace CampNative\Http\Controllers;

use CampNative\CampgroundPhoto;
use CampNative\Http\Requests;
use CampNative\Http\Controllers\Controller;

use CampNative\Services\CampgroundManager;
use CampNative\Services\CampgroundPhotoManager;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

class CampgroundPhotoController extends Controller {

    /**
     * @var Guard
     */
    public $auth;

    /**
     * @var CampgroundManager
     */
    public $campground;

    /**
     * @var CampgroundPhotoManager
     */
    public $manager;

    /**
     * New class instance
     *
     * @param Guard $auth
     * @param CampgroundManager $campground
     * @param CampgroundPhotoManager $manager
     */
    public function __construct(Guard $auth, CampgroundManager $campground, CampgroundPhotoManager $manager)
    {
        $this->auth = $auth;
        $this->campground = $campground;
        $this->manager = $manager;

        $this->middleware('auth');
        $this->middleware('hasUserRole');
    }

    /**
     * Shows a list of the campground photos uploaded by the user
     *
     * @param $campgroundId
     * @return \Illuminate\View\View
     */
    public function index($campgroundId)
    {
        $campground = $this->campground->find($campgroundId);

        $photos = CampgroundPhoto::whereUser($this->auth->user()->id)
            ->whereCampgroundId($campgroundId)
            ->latest()
            ->paginate(5);

        return view('campground.photo.index', compact('campground', 'photos'));
    }

    /**
     * Upload photo page/form
     *
     * @param $campgroundId
     * @return \Illuminate\View\View
     */
    public function create($campgroundId)
    {
        $campground = $this->campground->find($campgroundId);

        return view('campground.photo.create', compact('campground'));
    }

    /**
     * Uploads a new campground photo
     *
     * @param $campgroundId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($campgroundId, Request $request)
    {
        $campground = $this->campground->find($campgroundId);

        $this->validate($request, [
            'photo' => 'required|image'
        ]);

        // Store the photo if uploaded
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');

            // New Filename
            $file_name = str_random(12).'.'.$file->getClientOriginalExtension();

            $file->move('./uploads/campground/photos/'.$campgroundId.'/', $file_name);

            $campground->photos()->create(['name' => $file_name]);
        }

        flash()->success('You have successfully uploaded a new photo to this CampGround.');

        return redirect()->action('CampgroundPhotoController@index', [$campgroundId]);
    }

    /**
     * Delete photo from campground
     *
     * @param $campgroundId
     * @param $photoId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($campgroundId, $photoId)
    {
        $this->manager->delete($campgroundId, $photoId);

        flash()->success('You have successfully deleted a photo from this CampGround.');

        return redirect()->action('CampgroundPhotoController@index', [$campgroundId]);
    }

   /**
     * Show Campground page
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show($campgroundId)
    {
        $photos = CampgroundPhoto::whereUser($this->auth->user()->id)
            ->whereCampgroundId($campgroundId)
            ->latest()
            ->paginate(5);

        return view('campground.show', compact('photos'));
    }
}
