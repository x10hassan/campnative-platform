<?php namespace CampNative\Http\Controllers\Admin;

use CampNative\Http\Controllers\Controller;
use CampNative\User;
use CampNative\Campground;

class PagesController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('hasAdminRole');
    }

    /**
     * Admin Home/Dashboard view
     *
     * @return \Illuminate\View\View
     */
    public function home()
    {
        $users = User::latest()->paginate(10);
        $campgrounds = Campground::latest()->paginate(10);
        return view('admin.pages.home', compact('users', 'campgrounds'));
    }

}