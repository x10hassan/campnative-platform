<?php namespace CampNative\Http\Controllers\Admin;

use CampNative\Http\Controllers\Controller;
use CampNative\User;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;


class UsersController extends Controller {

    /**
     * @var Guard
     */
    public $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;

        $this->middleware('auth');
        $this->middleware('hasAdminRole');
    }

    /**
     * A list of the users
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $users = User::latest()->paginate(10);

        return view('admin.users.index', compact('users'));
    }

    /**
     * Allows the admin to switch into a user account.
     *
     * @param $userId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getManageAccount($userId, Request $request)
    {
        // Remember details about our current authenticated user
        session(['return_to_admin_id' => $this->auth->user()->id]);
        session(['return_to_admin_path' => $request->get('path')]);

        // Swap the authenticated user
        $this->auth->loginUsingId($userId);

        flash()->info('
            You are currently managing this user account from admin.
            To return to admin, click the link in the top menu.
        ');

        return redirect()->action('PagesController@home');
    }

    /**
     * Deletes a user by their ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id) 
    {
        // Delete the user account
        User::findOrFail($id)->delete();

        flash()->success('You have successfully deleted a user.');

        return redirect()->action('Admin\UsersController@index');
    }

}