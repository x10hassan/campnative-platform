<?php namespace CampNative\Http\Controllers\Admin;

use CampNative\Campground;
use CampNative\Http\Controllers\Controller;
use CampNative\User;
use Illuminate\Auth\Guard;

class CampgroundsController extends Controller {

    /**
     * @var Guard
     */
    public $auth;

    /**
     * @var Campground
     */
    public $campground;

    public function __construct(Guard $auth, Campground $campground)
    {
        $this->auth = $auth;
        $this->campground = $campground;

        $this->middleware('auth');
        $this->middleware('hasAdminRole');
    }

    /**
     * Shows a list of the campgrounds
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $campgrounds = $this->campground
            ->with(['user'])
            ->latest()
            ->paginate(1);

        return view('admin.campgrounds.index', compact('campgrounds'));
    }

    /**
     * Shows the edit campground form
     *
     * @param $campgroundId
     * @return \Illuminate\View\View
     */
    public function getEdit($campgroundId)
    {
        $campground = $this->campground
            ->with(['user'])
            ->findOrFail($campgroundId);

        return view('admin.campgrounds.edit', compact('campground'));
    }

    /**
     * Updates the campground and redirects upon success or fail
     *
     * @param $campgroundId
     */
    public function postEdit($campgroundId)
    {
        $campground = $this->campground
            ->with(['user'])
            ->findOrFail($campgroundId);

        // update the campground, validate, flash, redirect
    }

}