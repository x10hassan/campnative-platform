<?php
namespace CampNative\Http\Controllers;
use Auth;
use Redirect;
use Facebook;
use CampNative\Profile;
use CampNative\User;
use CampNative\Http\Requests;
use CampNative\Http\Controllers\Controller;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Input;

class ProfilesController extends Controller {

    public function __construct() {

        // $this->middleware('auth');
        //$this->middleware('hasUserRole');
    }

    public function home() {

        $code = Input::get('code');

        if (strlen($code) == 0){
            return Redirect::to('/') -> with('message', 'There was an error communicating with Facebook');
        }
            

        $facebook = new Facebook( array('appId' => env('FACEBOOK_APP_ID'), 'secret' => env('FACEBOOK_APP_SECRET')));

        $me = $facebook -> api('/me');

        $uid = $facebook -> getUser();

        if ($uid == 0)
            return Redirect::to('/') -> with('message', 'There was an error');

        $profile = Profile::whereUid($uid) -> first();
        $is_email = User::whereEmail($me['email'])->first();
        $user_id = "";
     if (empty($is_email)) {

            $user = new User;
            $user -> name = $me['first_name'] . ' ' . $me['last_name'];
            $user -> email = $me['email'];
            $user->password = md5(rand(0,121212121212121212));
            //$user->photo = 'https://graph.facebook.com/'.$me['username'].'/picture?type=large';

            $user -> save();
            $user_id = $user->id;
            $user->assignRole('user');
            $profile = new Profile();
            $profile -> uid = $uid;
            // $profile->username = $me['username'];
            $profile = $user -> profiles() -> save($profile);
        }else{
                if(empty($profile)){
                         //dd($user);
                        $profile = new Profile();
                        $profile -> uid = $uid;
                         $user_id = $is_email->id;
                        $profile->user_id = $is_email->id;
                        $profile->save();
                        
                    }else{
                     $user_id = $is_email->id;
                     }
        }
        $img = "http://graph.facebook.com/".$me['id']."/picture?type=large";
       
       
        copy($img,public_path()."/avatars/".$user_id.".png");
        $profile -> access_token = $facebook -> getAccessToken();
        $profile -> save();
        
        $user = $profile -> user;
        Auth::login($user);
       return Redirect::to('/home') -> with('message', 'Logged in with Facebook');
       // return Redirect::to('/') -> with('message', 'Logged in with Facebook');
        //return Redirect::to('/') -> with('message', 'Logged in with Facebook');
    }

}
