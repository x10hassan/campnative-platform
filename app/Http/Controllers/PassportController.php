<?php namespace CampNative\Http\Controllers;

use CampNative\Http\Requests;
use CampNative\Http\Controllers\Controller;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

class PassportController extends Controller {

    /**
     * @var Guard
     */
    public $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;

        $this->middleware('auth');
        $this->middleware('hasUserRole');
    }

    /**
     * Passport Profile page
     *
     * @return \Illuminate\View\View
     */
    public function getProfile()
    {
        return view('passport.profile');
    }

    /**
     * Passport Settings page
     *
     * @return \Illuminate\View\View
     */
    public function getSettings()
    {
        return view('passport.settings');
    }

    /**
     * Updates the user account settings
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSettings(Request $request)
    {
        $user = $this->auth->user();

        $this->validate($request, [
            'name' => 'required|max:200',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'password' => 'confirmed|min:6',
            'avatar' => 'image'
        ]);

        // Update the user
        $user->update($request->only('name', 'email'));

        // Save password if entered
        if ($request->has('password')) {
            $user->update(['password' => bcrypt($request->get('password'))]);
        }

        // Store avatar image if uploaded
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');

            // New Filename
            $file_name = $user->id.'.'.$file->getClientOriginalExtension();

            $file->move('./uploads/avatars/', $file_name);
            $user->update(['avatar_file' => $file_name]);
        }

        flash()->success('You have successfully updated your profile settings.');

        return redirect()->action('PassportController@getSettings');
    }

}
