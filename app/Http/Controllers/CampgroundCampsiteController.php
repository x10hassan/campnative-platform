<?php namespace CampNative\Http\Controllers;

use CampNative\CampgroundCampsite;
use CampNative\Http\Requests;
use CampNative\Http\Controllers\Controller;

use CampNative\Services\CampgroundManager;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

class CampgroundCampsiteController extends Controller {

    /**
     * @var Guard
     */
    public $auth;

    /**
     * @var CampgroundManager
     */
    public $campground;

    /**
     * New class instance
     *
     * @param Guard $auth
     * @param CampgroundManager $campground
     */
    public function __construct(Guard $auth, CampgroundManager $campground)
    {
        $this->auth = $auth;
        $this->campground = $campground;

        $this->middleware('auth');
        $this->middleware('hasUserRole');
    }

    /**
     * Lists the campgrounds campsites
     *
     * @param $campgroundId
     * @return \Illuminate\View\View
     */
	public function index($campgroundId)
	{
        $campground = $this->campground->find($campgroundId, $this->auth->user()->id);

        $campsites = CampgroundCampsite::whereCampgroundId($campgroundId)
            ->latest()
            ->paginate(5);

        return view('campground.campsite.index', compact('campground', 'campsites'));
	}

    /**
     * Display the campsite creation form
     *
     * @param $campgroundId
     * @return \Illuminate\View\View
     */
    public function create($campgroundId)
    {
        $campground = $this->campground->find($campgroundId, $this->auth->user()->id);

        return view('campground.campsite.create', compact('campground'));
    }

    /**
     * Store the campsite to the campground
     *
     * @param $campgroundId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($campgroundId, Request $request)
    {
        $campground = $this->campground->find($campgroundId, $this->auth->user()->id);

        $this->validate($request, [
            'name' => 'required|max:150',
            'address' => 'required|max:150',
            'phone' => 'required|max:150',
            'email' => 'required|email',
            'description' => 'max:300',
        ]);

        $campground->campsites()->create($request->only([
            'name', 'address', 'phone', 'email', 'description'
        ]));

        flash()->success('You have successfully created a new CampSite for this CampGround.');

        return redirect()->action('CampgroundCampsiteController@index', [$campgroundId]);
    }

    public function delete($campgroundId, $campsiteId)
    {
        $user_id = $this->auth->user()->id;
        $campground = $this->campground->find($campgroundId, $user_id);

        $campsite = CampgroundCampsite::with(['campground' => function($query) use ($campgroundId, $user_id) {
            $query->whereId($campgroundId);
            $query->whereUserId($user_id);
        }])->find($campsiteId);

        return redirect()->action('CampgroundCampsiteController@index', [$campgroundId]);
    }

}
