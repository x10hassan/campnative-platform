<?php

Route::get('/', function() {
    return redirect()->action('PagesController@home');
});

/**
 * ADMIN PANEL ROUTES
 */
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {

    Route::get('home', 'PagesController@home');

    /**
     * USER MANAGEMENT ROUTES FOR ADMIn
     */
    Route::get('users/{id}/manage', 'UsersController@getManageAccount');
    Route::resource('users', 'UsersController');

    Route::controllers([
        //'campgrounds' => 'CampgroundsController',
    ]);

});

/**
 * MISC WEBSITE PAGE ROUTES
 */
Route::get('/terms', 'PagesController@terms');
Route::get('/privacy', 'PagesController@privacy');

/**
 * USER ROUTES
 */
Route::get('home', 'PagesController@home');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/**
 * LIST CAMPGROUND ROUTES
 */
use Illuminate\Support\Facades\Session;

Route::get('/list-campground/session', function() {
    $session = Session::all();

    return $session;
});
Route::get('list-campground', 'CampgroundController@create');
Route::post('list-campground', 'CampgroundController@store');
Route::get('list-campground/store-long-and-lat', 'CampgroundController@storeLongAndLatSessions');
Route::get('list-campground/store-redirect', 'CampgroundController@storeAndRedirect');

/**
 * MANAGE CAMPGROUND ROUTES
 */
//Route::get('campground/photos', 'CampgroundPhotoController@index');
//Route::get('campground/photos/upload', 'CampgroundPhotoController@upload');

Route::get('campground/{campground}/photos/{photo}/delete', 'CampgroundPhotoController@delete');
Route::get('campground/{campground}/delete', 'CampgroundController@delete');
Route::get('campground/{campground}/campsites/{campsite}/delete', 'CampgroundCampsiteController@delete');
Route::resource('campground.campsites', 'CampgroundCampsiteController');
Route::resource('campground.photos', 'CampgroundPhotoController');
Route::resource('campground', 'CampgroundController');

/**
 * PASSPORT ROUTES
 */
Route::get('passport/settings', 'PassportController@getSettings');
Route::post('passport/settings', 'PassportController@postSettings');
Route::get('passport/profile', 'PassportController@getProfile');
Route::get('login/fb', function() {
    $facebook = new Facebook(array('appId'=>env('FACEBOOK_APP_ID'),'secret'=>env('FACEBOOK_APP_SECRET')));
    $params = array(
        'redirect_uri' => url('/login/fb/callback'),
        'scope' => 'email',
    );
    return Redirect::to($facebook->getLoginUrl($params));
});
Route::get('login/fb/callback','ProfilesController@home');