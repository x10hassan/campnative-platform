<?php namespace CampNative\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfNotUser {

    /**
     * Auth instance
     *
     * @var
     */
    public $auth;

    /**
     * User instance
     *
     * @var
     */
    public $user;

    /**
     * New class instance
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->user = $this->auth->user();
    }

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        // Redirect to a restricted page IF the currently
        // authenticated user does not have the "user" role.
        if ( ! $this->user->hasRole('user')) {

            if ($this->user->hasRole('admin'))
            {
                flash()->info('You are currently logged in as an Admin user.');

                return redirect()->action('Admin\PagesController@home');
            }

            $this->auth->logout();

            flash()->warning('You do not have sufficient privileges to access that page. Please relogin.');

            return redirect()->action('Auth\AuthController@getLogin');
        }

		return $next($request);
	}

}
