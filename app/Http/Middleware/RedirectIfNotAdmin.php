<?php namespace CampNative\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfNotAdmin {

    /**
     * Auth instance
     *
     * @var
     */
    public $auth;

    /**
     * User instance
     *
     * @var
     */
    public $user;

    /**
     * New class instance
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->user = $this->auth->user();
    }

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        // Redirect to a restricted page IF the currently
        // authenticated user does not have the "admin" role.
        if ( ! $this->user->hasRole('admin')) {

            $this->auth->logout();

            if ($request->ajax())
            {
                return response('Insufficient Privileges', 401);
            }

            flash()->warning('You do not have sufficient privileges to access that page. Please relogin.');

            return redirect()->action('Auth\AuthController@getLogin');
        }

		return $next($request);
	}

}
