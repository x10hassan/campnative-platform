<?php namespace CampNative;

use Illuminate\Database\Eloquent\Model;

class Campground extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'campgrounds';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	public $fillable = [
        'name',
        'address',
        'phone',
        'email',
        'description',
        'location_long',
        'location_lat',
    ];

    /**
     * Campground has many photos relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany('CampNative\CampgroundPhoto');
    }

    /**
     * Campground has many campsites
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function campsites()
    {
        return $this->hasMany('CampNative\CampgroundCampsite');
    }

    /**
     * User Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('CampNative\User');
    }

    /**
     * Scopes the eloquent query for a specific campground
     *
     * @param $query
     * @param $id
     * @return mixed
     */
    public function scopeWhereUser($query, $id)
    {
        return $query->whereUserId($id);
    }

}
