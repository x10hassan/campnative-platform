<?php 
namespace CampNative;
use Illuminate\Database\Eloquent\Model;
use CampNative\User;



class Profile extends Model {

   
    protected $table = 'profiles';
     public function user()
    {
        return $this->belongsTo('CampNative\User');
    }


}