<?php namespace CampNative\Services;

use CampNative\Campground;
use CampNative\CampgroundPhoto;
use Illuminate\Contracts\Auth\Guard;

use Storage;

class CampgroundPhotoManager {

    /**
     * @var Guard
     */
    public $auth;

    /**
     * @var Campground
     */
    public $campground;

    /**
     * @var CampgroundPhoto
     */
    public $photo;

    /**
     * New class instance
     *
     * @param Guard $auth
     * @param Campground $campground
     * @param CampgroundPhoto $photo
     */
    public function __construct(Guard $auth, Campground $campground, CampgroundPhoto $photo)
    {
        $this->auth = $auth;
        $this->campground = $campground;
        $this->photo = $photo;
    }

    public function delete($campgroundId, $photoId)
    {
        $photo = $this->photo->whereCampgroundId($campgroundId)->findOrFail($photoId);
        
        Storage::delete('./campground/photos/'.$campgroundId.'/'.$photo->name);

        return $photo->delete();
    }

}