<?php namespace CampNative\Services;

use CampNative\Campground;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Session\Store;

class CampgroundManager {

    /**
     * @var Guard
     */
    public $auth;

    /**
     * @var Store
     */
    public $session;

    /**
     * @var Campground
     */
    public $campground;

    /**
     * New class instance
     *
     * @param Guard $auth
     * @param Store $session
     * @param Campground $campground
     */
    public function __construct(Guard $auth, Store $session, Campground $campground)
    {
        $this->auth = $auth;
        $this->session = $session;
        $this->campground = $campground;
    }

    /**
     * Validation rules for a Campground
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:150',
            'address' => 'required|max:150',
            'phone' => 'required|max:150',
            'email' => 'required|email',
            'description' => 'max:300'
        ];
    }

    /**
     * Stores the campground input (from the form on the page) to session
     * so we can use it to store to the database later
     *
     * @param array $data
     */
    public function storeToSession(array $data)
    {
        $this->session->put('campground_pending', true);
        $this->session->put('campground_name', $data['name']);
        $this->session->put('campground_address', $data['address']);
        $this->session->put('campground_phone', $data['phone']);
        $this->session->put('campground_email', $data['email']);
    }

    /**
     * Creates a Campground using the session data
     *
     * @return mixed
     */
    public function createFromSession()
    {
        $this->session->forget('campground_pending');

        $data = [
            'name' => $this->session->pull('campground_name'),
            'address' => $this->session->pull('campground_address'),
            'phone' => $this->session->pull('campground_phone'),
            'email' => $this->session->pull('campground_email')
        ];

        $campground = $this->auth->user()->campgrounds()->create($data);

        // If theres a location longitude and latitude, we need to save that
        if ($this->session->has('location_long') && $this->session->has('location_lat'))
        {
            $campground->location_long = $this->session->pull('location_long');
            $campground->location_lat = $this->session->pull('location_lat');
            $campground->save();
        }

        return $campground;
    }

    /**
     * Find the campground by ID or fail
     *
     * @param $id
     * @param bool $userId
     * @return mixed
     */
    public function find($id, $userId = false)
    {
        if ($userId) {
            return $this->campground->whereUserId($userId)->findOrFail($id);
        }

        return $this->campground->findOrFail($id);
    }

    /**
     * Deletes the campground from the database
     *
     * TODO: Delete all stored photos for this campground
     *
     * @param $id
     * @return mixed
     */
    public function delete($id) 
    {
         return $this->find($id)->delete();
    }

}