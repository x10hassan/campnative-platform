<?php namespace CampNative;

use Torann\Promise\HasRole;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword;

    /**
     * Torann\Promise\HasRole
     */
    use HasRole;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar_file'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Additional date columns
     *
     * @var array
     */
    protected $dates = ['last_login'];

    public function getLastLogin()
    {
        return $this->last_login->timestamp <= 0 ? 'Never' : $this->last_login->diffForHumans();
    }

    /**
     * Campgrounds Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function campgrounds()
    {
        return $this->hasMany('CampNative\Campground');
    }

    public function profiles()
    {
        return $this->hasMany('CampNative\Profile');
    }

}
