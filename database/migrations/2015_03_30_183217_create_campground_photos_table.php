<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampgroundPhotosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campground_photos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('campground_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('campground_id')->references('id')->on('campgrounds')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campground_photos');
    }

}
