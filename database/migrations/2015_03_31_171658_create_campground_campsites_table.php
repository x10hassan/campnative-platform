<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampgroundCampsitesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('campground_campsites', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('campground_id')->unsigned();
            $table->string('name');
            $table->string('address');
            $table->string('email');
            $table->string('phone');
            $table->timestamps();

            $table->foreign('campground_id')->references('id')->on('campgrounds')->onDelete('cascade')->onUpdate('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('campground_campsites');
	}

}
