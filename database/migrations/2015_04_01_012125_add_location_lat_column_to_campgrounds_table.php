<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationLatColumnToCampgroundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campgrounds', function(Blueprint $table) {
            $table->string('location_lat');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('campgrounds', function(Blueprint $table) {
            $table->dropColumn('location_lat');
        });
	}

}
