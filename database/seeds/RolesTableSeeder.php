<?php

use Illuminate\Database\Seeder;
use Torann\Promise\Models\Role;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class RolesTableSeeder extends Seeder {

    public function run()
    {
        $admin = new Role;
        $admin->name = 'admin';
        $admin->description = 'Administrator';
        $admin->save();

        $user = new Role;
        $user->name = 'user';
        $user->description = 'User';
        $user->save();
    }

}