<?php

use Illuminate\Database\Seeder;
use CampNative\User;

class UsersTableSeeder extends Seeder {

    public function run()
    {
        $justin = new User;
        $justin->name = 'Justin';
        $justin->email = 'grindmodeon@gmail.com';
        $justin->password = bcrypt('cntest');
        $justin->save();

        $justin->assignRole('admin');

        $john = new User;
        $john->name = 'John Doe';
        $john->email = 'john@doe.com';
        $john->password = bcrypt('cntest');
        $john->save();

        $john->assignRole('user');
    }

}